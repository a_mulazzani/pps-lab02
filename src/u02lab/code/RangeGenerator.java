package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int start;
    private int stop;
    private int currentNumber;
    private boolean isOver = false;


    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.currentNumber = start;
    }

    @Override
    public Optional<Integer> next() {
        if (currentNumber == stop)
        {
            this.isOver = true;
        }
        return ( isOver() && currentNumber > stop)  ? Optional.empty() : Optional.of(currentNumber++);
    }

    @Override
    public void reset() {
        this.currentNumber = this.start;
        this.isOver = false;
    }

    @Override
    public boolean isOver() {
        return isOver;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingNumbers = new ArrayList<>();
        while (!isOver())
        {
            remainingNumbers.add(next().get());
        }
        return Collections.unmodifiableList(remainingNumbers);
    }
}

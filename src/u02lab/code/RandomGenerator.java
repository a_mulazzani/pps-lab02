package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int maxNumberOfBits;
    private int currentBit = 0;
    private boolean isOver = false;


    public RandomGenerator(int maxNumberOfBits){
        this.maxNumberOfBits = maxNumberOfBits;
    }

    @Override
    public Optional<Integer> next() {

        if (isOver())
        {
            return Optional.empty();
        }
        if (++currentBit == maxNumberOfBits)
        {
          isOver = true;   
        }
        return Math.random() > 0.5 ? Optional.of(1) : Optional.of(0);


    }

    @Override
    public void reset() {

    this.currentBit = 0;
    this.isOver = false;
    }

    @Override
    public boolean isOver() {
        return isOver;
    }

    @Override
    public List<Integer> allRemaining() {

        List<Integer> remainingNumbers = new ArrayList<>();
        while (!isOver()){
            remainingNumbers.add(next().get());
        }
        return Collections.unmodifiableList(remainingNumbers);
    }
}

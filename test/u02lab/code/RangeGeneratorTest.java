package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by alberto.mulazzani on 07/03/2017.
 */
public class RangeGeneratorTest {
    private static final int START = 1;
    private static final int STOP = 10;
    private SequenceGenerator generator;


    @Before
    public void setup(){
        this.generator = new RangeGenerator(START,STOP);
    }

    @Test
    public void next() throws Exception {

        for ( int i = START; i <= STOP; i++){
            assertTrue(generator.next().equals(Optional.of(i)));
        }

    }

    @Test
    public void reset() throws Exception {
        generator.next();
        generator.reset();
        assertTrue(generator.next().equals(Optional.of(START)));

    }

    @Test
    public void isOver() throws Exception {
        assertFalse(generator.isOver());
        for (int i = START; i <= STOP; i++){
            generator.next();
        }

        generator.next();
        assertTrue(generator.isOver());

    }

    @Test
    public void checkAllRemainingStart() throws Exception {

        assertEquals(getRemainingNumbers(START), generator.allRemaining());


    }

    @Test
    public void checkAllRemainingMidway() throws Exception {

        generator.next();
        generator.next();
        assertEquals(getRemainingNumbers(START + 2), generator.allRemaining());
    }

    private List<Integer> getRemainingNumbers(int start){
        List<Integer> remainingNumbers = new ArrayList<>();
        for (int i = start; i <= STOP; i++){
            remainingNumbers.add(i);
        }
        return remainingNumbers;

    }
}
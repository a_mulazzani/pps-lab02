package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by alberto.mulazzani on 07/03/2017.
 */
public class RandomGeneratorTest {
    private static final int bitsToGenerate = 5;
    private SequenceGenerator generator;


    @Before
    public void setUp() throws Exception {
    generator = new RandomGenerator(bitsToGenerate);
    }

    @Test
    public void nextIsInRange() throws Exception {
        while(!generator.isOver()) {
            Optional<Integer> value = generator.next();
            assertTrue((value.equals(Optional.of(0)) || value.equals(Optional.of(1))));

        }

    }

    @Test
    public void nextIsIsBounds() throws Exception {

        assertEquals(bitsToGenerate, getNumberOfIteration() );
    }

    @Test
    public void reset() throws Exception {
        generator.next();
        generator.next();
        assertNotEquals(bitsToGenerate, getNumberOfIteration() );
        generator.reset();
        assertEquals(bitsToGenerate, getNumberOfIteration() );
    }

    @Test
    public void isOver() throws Exception {
        for (int i = 0; i < bitsToGenerate; i++){
            generator.next();
        }

        assertTrue(generator.isOver());
    }

    @Test
    public void allRemainingTotal() throws Exception {
        assertEquals(bitsToGenerate, generator.allRemaining().size());
    }

    @Test
    public void allRemainingMidway() throws Exception {
        generator.next();
        generator.next();

        assertEquals(bitsToGenerate - 2, generator.allRemaining().size());
    }

    private int getNumberOfIteration(){
        int currentIteration = 0;
        while (!generator.isOver()){
            currentIteration++;
            generator.next();
        }
        return currentIteration;
    }

}